package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	private static int maxItems = 20;
	private ArrayList<FridgeItem> itemList;
	
	public Fridge() {
		this.itemList = new ArrayList<>();
	}
	
	
	public int nItemsInFridge() {
		return itemList.size();
		
	}


	public int totalSize() {
		return maxItems;
	}


	public boolean placeIn(FridgeItem item){
		if (nItemsInFridge() < maxItems) {
			itemList.add(item);
			return true;
		} else {
			return false;
		}
	}

	public void takeOut(FridgeItem item) {
		if (nItemsInFridge() > 0) {
			itemList.remove(item);
		} else {
			throw new NoSuchElementException();
		}
	}


	public void emptyFridge() {
		itemList.clear();
	}

	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredList = new ArrayList<>();
		for(FridgeItem item : itemList) {
			if (item.hasExpired()) {
				expiredList.add(item);
			}
		}
		itemList.removeAll(expiredList);
		return expiredList;	
	}
}
